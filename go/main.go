package main

import (
	"auto-affect/pkg"
	"fmt"
)

const url_base = "https://preprod.overbookd.24heures.org/api"

func main() {
	token, err := pkg.GetToken()
	if err != nil {
		fmt.Println(err)
	}
	// if s, ok := extract.(ApiToken); ok {
	// 	fmt.Println(s.Token)
	// }
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(token)

	users, err := pkg.GetUsers(token)

	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(users)

	fts, err := pkg.GetFTs(token)

	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(fts)

	usersObjects, err := pkg.SetUsers(token, users)
	if err != nil {
		fmt.Println(err)
	}
	_ = usersObjects
	//fmt.Println(usersObjects)
	ftObjects, err := pkg.SetSlots(token, fts)
	fmt.Println(ftObjects)
}
