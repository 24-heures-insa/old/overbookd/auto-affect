const Volunteer = require('./classes.js').Volunteer;
const Slot = require('./classes.js').Slot;
const Assignment = require('./classes.js').Assignment;

const credentials = require('./config.js');
const qs = require("querystring");
var Logger = require('js-logger');
const fs = require('fs')

const axios = require('axios');

const { PerformanceObserver, performance } = require('perf_hooks');

const obs = new PerformanceObserver((items) => {
    console.log(items.getEntries()[0].duration);
    performance.clearMarks();
});

var token = "";
var instanceAPI = "";
const instanceAuth = axios.create({
    baseURL: 'https://preprod.overbookd.24heures.org/api',
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
});
const data = qs.stringify({
    // keycloak accepts www-urlencoded-form and not users
    username: credentials.username,
    password: credentials.password,
});
// Constants for annealing function
const initial_temperature = 15000;
const temperature_length = 1500;
const cooling_ratio = 0.99;
const final_temperature = 1e-5;
//Constants for genetic algorithm
const population_size = 1000;
const generations = 200;
const recombination_rate = 0.5;
const mutation_rate = 0.001;


const noVolunteerAssigned = 10000;
const notAvailablePenalty = 1000; //OK
const conflictPenalty = 1000; //OK
const notRequiredVolunteerPenalty = 1000; //OK
const notNimimumRolePenalty = 800; // OK
const notEnoughSleepTimePenalty = 500;
const noMinimalPausePenalty = 200;
const humanRequiredPenalty = 150;
const cannotCarryHeavyPenalty = 100;
const noFriendPenalty = 8;
const notVaryTasksPenalty = 3;
const tooMuchSlotsPenalty = 1;
const notEnoughSlotsPenalty = 2;

const initialScore = 1000000;

function removeItemOnce(arr, value) {
    var index = arr.indexOf(value);
    if (index > -1) {
        arr.splice(index, 1);
    }
    return arr;
}

function getToken(){
    return instanceAuth.post('/login', data)
        .then(function (response) {
            token = "Bearer " + response.data.token;
        })
        .catch(function (error) {
            console.log(error);
        });
}

async function getUser() {
    try {
        const data = await getToken();
        instanceAPI = axios.create({
            baseURL: 'https://preprod.overbookd.24heures.org/api',
            timeout: 2000,
            headers:{'Authorization': token}
        });
        const response = await instanceAPI.get('/user');
        return response.data;
    } catch (error) {
        console.error(error);
    }
}

async function setUsers(localUserDb = null){
    try{
        let volunteers = [];
        console.log(localUserDb==null);
        let users = null;
        if(localUserDb != null) { //In case of local DB use
            users = localUserDb;
            console.log("userDbNotNull");
            for (let i = 0; i < users.length; i++) {
                volunteers = users.map(u =>
                    new Volunteer(u.firstname, u.lastname, u.nickname, u.team, u.friends, u._id,
                    u.isCouple, u.sex, u.email, u.phone, u.handicap, u.hasDriverLicense, u.driverLicenseDate, u.birthdate,
                    u.departement, u.year, u.availabilities));
                return volunteers;
            }
        }
        else {
            users = await getUser();
            timeslots = await getTimeslots();

            let timeslotsAvailabilitiesTab = [];
            for (let i = 0; i < users.length; i++) {
                let timeslotsAvailabilities = [];
                for (let j = 0; j < users[i].availabilities.length; j++) {
                    console.log(users[i].availabilities[j]);
                    let timeslot = timeslots.find(t => t._id === users[i].availabilities[j]);
                    timeslotsAvailabilities.push(timeslot);
                }
                timeslotsAvailabilitiesTab.push(timeslotsAvailabilities);
            }
            volunteers = users.map(u => new Volunteer(u.firstname, u.lastname, u.nickname, u.team, u.friends, u._id,
                u.isCouple, u.sex, u.email, u.phone, u.handicap, u.hasDriverLicense, u.driverLicenseDate, u.birthdate,
                u.departement, u.year, u.availabilities));

            for (let i = 0; i < users.length; i++) {
                users[i].availabilities = timeslotsAvailabilitiesTab[i];
            }

            return volunteers;
        }
    } catch (error){
        console.log(error);
    }
};

async function getFT(){
    let data = await getToken();
    instanceAPI = axios.create({
        baseURL: 'https://preprod.overbookd.24heures.org/api',
        timeout: 2000,
        headers:{'Authorization': token}
    });
    const response = await instanceAPI.get('/FT');
    return response.data.data;
}

async function getTimeslots(){
    let data = await getToken();
    instanceAPI = axios.create({
        baseURL: 'https://preprod.overbookd.24heures.org/api',
        timeout: 2000,
        headers:{'Authorization': token}
    });
    const response = await instanceAPI.get('/timeslot');
    return response.data;
}

async function setSlots(){
    try{
        const FT = await getFT();
        let slots = [];
        for (let i = 0; i<FT.length; i++){
            try{
                for (let j=0; j<FT[i].timeframes.length; j++) {
                    if (FT[i].timeframes[j].required !== undefined) {
                        for (let k = 0; k < FT[i].timeframes[j].required.length; k++) {
                            for (let l = 0; l < FT[i].timeframes[j].required[k].amount; l++) {
                                let newSlot = new Slot(FT[i]._id, FT[i].general.name, FT[i].instructions, FT[i].general.inCharge, FT[i].general.location, FT[i].status,
                                    "", FT[i].timeframes[j].start, FT[i].timeframes[j].end, FT[i].timeframes[j].required[k].type, FT[i].timeframes[j].required[k].team, FT[i].timeframes[j].required[k].user, FT[i].general.driverLicenseRequired);
                                slots.push(newSlot);
                            }
                        }
                    }
                }
            } catch (e) {
                console.log("Cette FT ne contient pas de créneau" + FT[i].general.name);
                console.log(e);
            }

        }
        return slots;
    } catch (error){
        console.log(error);
    }
}

function assignSlotToVolunteer(id, volunteer, slot){
    let assignment = new Assignment(id, volunteer, slot);
    var dStart = new Date(0);
    var dEnd = new Date(0);
    dStart.setUTCMilliseconds(slot.start);
    dEnd.setUTCMilliseconds(slot.end);
    //console.debug("New Assignment : \n User : " + volunteer.firstname+ " " + volunteer.lastname +"\n Slot : " + slot.name +"\n Time : "+ dStart + " to " + dEnd + "\n Required type and team : " + slot.requiredType + " " + slot.requiredTeam);
    return assignment;
}

function getObjectById(tab, id){
    for (let i = 0; i < tab.length; i++){
        if (tab[i]._id === id){
            return tab[i];
        }
    }
}

// Penalties functions
function isConflictdOnSlot(assignment, assignments){
    conflict_counter = 0;
    //if volunteer is undefined, raise an error and return false
    for (let i = 0; i < assignments.length; i++) {
        if (assignments[i].volunteer === undefined){
            //Do nothing
        }
        else{
            if( assignment.volunteer.id === assignments[i].volunteer.id && assignment.slot.start === assignments[i].slot.start && assignment.slot.end === assignments[i].slot.end){
                conflict_counter += 1;
                if(conflict_counter > 1){
                    return true;
                }
            }
        }
    }
    return false;
}

function isWrongRequiredVolunteer(assignment, volunteers){
    try {
        if (assignment.slot.requiredType === "user") {
            if (assignment.volunteer !== getObjectById(volunteers, assignment.slot.requiredUser._id)) {
                return true;
            }
        }
    }catch (e) {
        //console.debug("No ID for required user");
        return true;
    }
    return false;
}

function hasFriendInSlot(assignment, assignments){
    volunteer_friends = []
    for (let i = 0; i < volunteer.friends.length ; i++) {
        volunteer_friends.push(volunteer.friends[i].id);
    }
    for (let i = 0; i < assignments.length; i++) {
        if(volunteer_friends.includes(assignments[i].volunteer.id) && assignment.id === assignments[i].id){
            return true;
        }
    }
    return false;
}

function isAvailable(volunteer, slot){
    try{
        for (let i = 0; i < volunteer.availabilities.length; i++) {
            if((volunteer.availabilities[i].timeFrame.start <= slot.start) && (volunteer.availabilities[i].timeFrame.end >= slot.end)){
                return volunteer.availabilities[i]; //Normally there is only one slot which is possible so we can return directly
                //We return it to pop it from user availabilities after it is assigned
            }
            //console.debug(volunteer.firstname + " " + volunteer.lastname + " Has no availability");
        }
    } catch (e) {
        //console.log(volunteer.firstname + " " + volunteer.lastname + " Has no availability");
    }
}

function isDriverLicenseStatusOk(volunteer, assignment){
    let today = new Date();
    var year = today.getFullYear();
    var month = today.getMonth();
    var day = today.getDate();
    let oneYearAgo = new Date(year - 1, month, day);
    if(assignment.driverLicenseRequired === "conducteur de Fen"){
        if(volunteer.team.includes("barrieres") && volunteer.hasDriverLicense){
            return true;
        }
        return false;
    }
    if(assignment.driverLicenseRequired === "permis - un ans"){
        if(volunteer.hasDriverLicense) {
            return true;
        }
        return false;
    }
    if (assignment.driverLicenseRequired === "permis + un ans"){
        if(volunteer.hasDriverLicense && volunteer.driverLicenseDate < oneYearAgo){
            return true;
        }
        return false;
    }
    return true;
}

function hasUserEnoughRole(volunteer, role){
    if (role === undefined){
        return true;
    }
    let volunteerRoles = volunteer.team;
    //Most basic role : soft
    if (role === "soft"){
        return true;
    }
    // TODO : Create confiance role on overbookd
    // Either a confiance or a hard can do confiance slot
    if (role === "confiance"){
        if ((volunteerRoles.includes("confiance") | (volunteerRoles.includes("hard")))){
            return true;
        }
        return false;
    }
    // Other roles are indicated
    if(volunteerRoles.includes(role)){
        return true;
    }
    return false;
}

function isAVolunteerAssigned(assignment){
    if (assignment.volunteer === undefined){
        return false;
    }
    return true;
}

function workingTab(volunteers, assignments, start, end) {
    let theWorkingTab = [];
    volunteers.forEach(volunteer => {
        let volWorkingTab = [];
        for (let i = start; i <= end; i += 7200000) {
            volWorkingTab.push(1);
            let not_found = true;
            current_start = i;
            current_end = current_start + 7200000;
            assignments.forEach(assignment => {
                if (assignment.slot.start === current_start && assignment.slot.end === current_end && assignment.volunteer.id === volunteer) {
                    volWorkingTab.push(1);
                    not_found = false;
                }
            });
            if (not_found) {
                volWorkingTab.push(0);
            }
        }
        theWorkingTab.push(volWorkingTab);
    });
    console.log(theWorkingTab);
}


async function assignMandatoryVolunteers(slots, volunteers) {
    newAssignments = [];
    let slots_unassigned = slots;
    let volunteers_updated = volunteers;
    slots.filter(s => s.requiredType == "user").forEach(slot => {
        volunteer = getObjectById(volunteers, slot.requiredUser._id);
        console.log(slot.requiredUser.username);
        availability = isAvailable(volunteer, slot);
        if (availability != null) {
            newAssignments.push(assignSlotToVolunteer("id", volunteer, slot));
            removeItemOnce(slots_unassigned, slot); //Remove slot from slots to assign
            for (let i = 0; i < volunteers_updated.length; i++) {
                if(volunteers[i] === volunteer){
                    removeItemOnce(volunteers_updated[i].availabilities, availability);
                }
            }
        } else {
            console.log("! Mandatory Assignment not assigned ! \n ");
        }
    });
    return {
        newAssignments,
        slots_unassigned,
        volunteers_updated
    };
}
async function assignVolunteersForStart(slots, volunteers, random=true){
    newAssignments = [];
    let slots_unassigned = slots;
    let volunteers_updated = volunteers;
    if(random === false) {
        slots.filter(s => s.requiredType === "team").forEach(slot => {
            let assigned = false;
            var dStart = new Date(0);
            dStart.setUTCMilliseconds(slot.start);
            console.log("NAME : " + slot.name + " " + dStart);
            console.log("requiredTeam : " + slot.requiredTeam);
            volunteers.forEach(volunteer => {
                if (assigned === false) {
                    availability = isAvailable(volunteer, slot);
                    if (availability != null) {
                        newAssignments.push(assignSlotToVolunteer("id", volunteer, slot));
                        removeItemOnce(slots_unassigned, slot); //Remove slot from slots to assign
                        removeItemOnce(volunteer.availabilities, availability); //Remove availability from user availabilities
                        assigned = true;
                    }
                }
            })
        });
    } else {
        //put a random volunteer to each slot
        slots.forEach(slot =>{
            newAssignments.push(assignSlotToVolunteer("id", volunteers[Math.floor(Math.random() * volunteers.length)], slot));
        });
    }
    return {
        newAssignments,
        slots_unassigned,
        volunteers
    };
}

async function autoAssign(slots, volunteers){
    startTime = performance.now();
    assignments = [];
    const start_assignments = await assignVolunteersForStart(slots, volunteers, true);
    start_assignments.newAssignments.forEach(ass => {
       assignments.push(ass);
    });
    optimized_assignments = await annealingFunction(final_temperature, temperature_length, 1, initial_temperature, cooling_ratio, assignments, volunteers, slots);
    const endTime = performance.now();
    time_taken = (endTime - startTime)/1000;
    console.log("Time taken : " + time_taken + "s");
    fs.appendFile("logs/annealing_values_" + initial_temperature + "-" + temperature_length + "-" + cooling_ratio.toString() +  ".txt", "Time taken : " + time_taken + "\n", (err) => {
        if (err) throw err;
    });
    return optimized_assignments;
}

//Annealing Algorithm
async function annealingFunction(stopConditionNumber, iterationNumber, changeNumber, initialTemp, coolingRatio, assignments, volunteers, slots){
    let t = initialTemp;
    let delta = 0;
    let actual_score = await objectiveFunction(assignments, volunteers, slots);
    let scores = []
    let currentDate = (new Date().getDate()).toString();
    fs.file
    fs.writeFile('logs/annealing_values_'+ initialTemp + "-" + iterationNumber + "-" + coolingRatio.toString() +  ".txt", "", (err) => {
        if (err) throw err;
    })
    fs.writeFile('logs/annealing_results_'+ initialTemp + "-" + iterationNumber + "-" + coolingRatio.toString() +  ".txt", "", (err) => {
        if (err) throw err;
    })
    while (t > stopConditionNumber){
        console.log("Actual temperature : " + t);
        for (let i = 0; i < iterationNumber; i++) {
            //console.debug("Nombre iterations : "+ i);
            let new_assignments = await neighborFunction(changeNumber, assignments);
            let new_score = await objectiveFunction(new_assignments, volunteers, slots);
            scores.push(actual_score);
            //remove points from string

            fs.appendFile('logs/annealing_values_' + initialTemp + "-" + temperature_length + "-" + cooling_ratio.toString() + '.txt', actual_score+"\n", (err) => {

                // In case of a error throw err.
                if (err) throw err;
            })
            delta =  actual_score - new_score;
            if(delta < 0){
                assignments = new_assignments;
                actual_score = new_score;
            }
            else{
                let x = Math.random();
                if(x < Math.exp(-delta/t)){
                    assignments = new_assignments;
                    actual_score = new_score;
                }
            }
        }
        t = coolingRatio * t;
    }
    fs.appendFile('logs/annealing_results_' + initialTemp + "-" + iterationNumber + "-" + coolingRatio.toString() + '.txt', JSON.stringify(assignments), (err) => {

        // In case of a error throw err.
        if (err) throw err;
    })
    console.log(assignments);
    fs.appendFile("logs/annealing_values_" + initialTemp + "-" + iterationNumber + "-" + coolingRatio.toString() +  ".txt", "Final Score : " + actual_score + "\n", (err) => {
        if (err) throw err;
    });
    return assignments;
}
async function neighborFunction(changeNumber, assignments){
    assignments_copy = assignments;
    for (let i = 0; i < changeNumber; i++) {
        var random1 = Math.floor(Math.random()*assignments_copy.length);
        var random2 = Math.floor(Math.random()*assignments_copy.length);
        while(random1 == random2 || random1 < 0 || random2 <0){
            random1 = Math.floor(Math.random()*assignments_copy.length);
            random2 = Math.floor(Math.random()*assignments_copy.length);
        }
        var assignment1 = assignments_copy[random1];
        var assignment2 = assignments_copy[random2];
        new_ass1 = assignSlotToVolunteer("id", assignment1.volunteer, assignment2.slot);
        new_ass2 = assignSlotToVolunteer("id", assignment2.volunteer, assignment1.slot);
        removeItemOnce(assignments_copy, assignment1);
        removeItemOnce(assignments_copy, assignment2);
        assignments_copy.push(new_ass1);
        assignments_copy.push(new_ass2);
    }
    return assignments_copy;
}

//Genetic Algorithm
async function makePopulation(volunteers, slots, populationNumber){
    const volunteer_number = volunteers.length;
    const slot_number = slots.length;
    const binary_volunteer_length = Math.ceil(Math.log2(volunteer_number));
    let population = [];
    for (let i = 0; i < populationNumber; i++) {
        let binary_string = "";
        for (let j = 0; j < slot_number; j++) {
            //Create a random binary number with the length of binary_volunteer_length
            for (let k = 0; k < binary_volunteer_length; k++){
                //append to binary_string
                binary_string += Math.floor(Math.random() * 2);
            }
        }
        population.push(binary_string);
    }
    return population;
}
async function binaryToAssignment(binary_string, volunteers, slots){
    let assignments = [];
    let binary_string_copy = binary_string;
    const binary_volunteer_length = Math.ceil(Math.log2(volunteers.length));
    for (let i = 0; i < slots.length; i++) {
        let volunteer_id_binary = binary_string_copy.toString().substring(0, binary_volunteer_length);
        let volunteer_id = parseInt(volunteer_id_binary, 2);
        binary_string_copy = binary_string_copy.toString().substring(binary_volunteer_length+1);
        assignments.push(assignSlotToVolunteer("id", volunteers[volunteer_id], slots[i]));
    }
    return assignments;
}
async function assignmentToBinary(assignments, volunteers, slots){
    let binary_string = "";
    const binary_volunteer_length = Math.ceil(Math.log2(volunteers.length));
    for (let i = 0; i < assignments.length; i++) {
        let volunteer_id = volunteers.indexOf(assignments[i].volunteer);
        let volunteer_id_binary = volunteer_id.toString(2);
        while(volunteer_id_binary.length < binary_volunteer_length){
            volunteer_id_binary = "0" + volunteer_id_binary;
        }
        // if the binary contains a -
        if(volunteer_id_binary.indexOf("-") > -1){
            volunteer_id_binary = "";
            for (let k = 0; k < binary_volunteer_length; k++) {
                //Replace undefined by new random number
                volunteer_id_binary += Math.floor(Math.random() * 2);
            }
        }
        binary_string += volunteer_id_binary;
    }
    return binary_string;
}
async function evolve(population, volunteers, slots, recombinationRate, mutationRate){
    //Population is an array of binary strings, we have to evaluate with fitness function each string
    //We have to make a new population with the best individuals from the old population
    let new_population = [];
    let fitness_values = [];
    for (let i = 0; i < population.length; i++) {
        let assignments = await binaryToAssignment(population[i], volunteers, slots);
        let fitness_value = await objectiveFunction(assignments, volunteers, slots);
        fitness_values.push(fitness_value);
        //console.log("Fitness value: " + fitness_value);
    }
    //log the best fitness value
    console.log("Best fitness value: " + Math.max(...fitness_values));
    //put this value in a file logs/genetic_fitness_values_populationsize_generations_recombinationrate_mutationrate.txt
    fs.appendFileSync("logs/genetic_values_" + population.length + "-" + generations + "-" + recombinationRate + "-" + mutationRate +".txt", Math.max(...fitness_values)+ "\n");
    //We put the worst fitness value as 0 and deduce this value from the others fitness values
    let worst_fitness_value = Math.min(...fitness_values);
    for (const fitness_value in fitness_values) {
        fitness_values[fitness_value] -= worst_fitness_value + 1;
    }
    //We treat the fitness values as probabilities
    let probabilities = [];
    let sum = 0;
    for (let i = 0; i < fitness_values.length; i++) {
        sum += fitness_values[i];
    }
    for (let i = 0; i < fitness_values.length; i++) {
        probabilities.push(fitness_values[i] / sum);
    }
    //We create a new population with the best individuals from the old population
    for (let i = 0; i < population.length; i++) {
        let random_number = Math.random();
        let sum = 0;
        for (let j = 0; j < probabilities.length; j++) {
            //We add the probability of the current individual to the sum
            sum += probabilities[j];
            //If the random number is lower than the sum, we have found the individual we want
            if (random_number < sum) {
                //We add the individual to the new population
                new_population.push(population[j]);
                break;
            }
        }
    }
    //We recombine the new population
    for (let i = 0; i < new_population.length; i++) {
        if (Math.random() < recombinationRate) {
            //We pick two random individuals
            let random_individual_1 = Math.floor(Math.random() * new_population.length);
            let random_individual_2 = Math.floor(Math.random() * new_population.length);
            //We recombine the two individuals
            new_population[i] = await recombine(new_population[random_individual_1], new_population[random_individual_2]);
        }
    }
    //We mutate the new population
    for (let i = 0; i < new_population.length*new_population[0].length; i++) {
        if (Math.random() < mutationRate) {
            //We pick a random individual
            let random_individual = Math.floor(Math.random() * new_population.length);
            //We mutate the individual
            new_population[random_individual] = await mutate(new_population[random_individual]);
        }
    }
    return new_population;
}
async function recombine(individual_1, individual_2){
    //We pick a random point in the binary string
    let random_point = Math.floor(Math.random() * individual_1.length);
    //We recombine the two individuals
    let new_individual = individual_1.substring(0, random_point) + individual_2.substring(random_point);
    return new_individual;
}
async function mutate(individual){
    //We pick a random point in the binary string
    let random_point = Math.floor(Math.random() * individual.length);
    //We flip the bit at the random point
    let new_individual = individual.substring(0, random_point) + (individual[random_point] == "0" ? "1" : "0") + individual.substring(random_point + 1);
    return new_individual;
}

async function geneticAlgorithm(volunteers, slots, populationSize, generations, recombinationRate, mutationRate){
    let startTime = performance.now();
    let population = await makePopulation(volunteers, slots, populationSize);
    fs.writeFile("logs/genetic_values_" + populationSize + "-" + generations + "-" + recombinationRate + "-" + mutationRate + ".txt", "", (err) => {
        if (err) throw err;
    });
    for (let i = 0; i < generations; i++) {
        console.log(`Generation ${i + 1}`);
        population = await evolve(population, volunteers, slots, recombinationRate, mutationRate);
    }

    //get the best individual by calculating the score
    let best_individual = await binaryToAssignment(population[0], volunteers, slots);
    let best_score = await objectiveFunction(best_individual, volunteers, slots);
    for (let i = 0; i < population.length; i++) {
        let assignments = await binaryToAssignment(population[i], volunteers, slots);
        let fitness_value = await objectiveFunction(assignments, volunteers, slots);
        if (fitness_value > best_score) {
            best_individual = assignments;
            best_score = fitness_value;
        }
    }
    console.log("Best individual score in last generation : " + best_score);
    fs.appendFile("logs/genetic_values_" + populationSize + "-" + generations + "-" + recombinationRate + "-" + mutationRate + ".txt", best_score + "\n", (err) => {
        if (err) throw err;
    });
    fs.writeFile("logs/genetic_results_" + populationSize + "-" + generations + "-" + recombinationRate + "-" + mutationRate + ".txt", JSON.stringify(best_individual), (err) => {
        if (err) throw err;
    });
    const endTime = performance.now();
    time_taken = (endTime - startTime)/1000;
    console.log("Time taken : " + time_taken + "s");
    fs.appendFile("logs/genetic_values_" + populationSize + "-" + generations + "-" + recombinationRate + "-" + mutationRate + ".txt", "Final Score : " + best_score + "\n", (err) => {
        if (err) throw err;
    });
    fs.appendFile("logs/genetic_values_" + populationSize + "-" + generations + "-" + recombinationRate + "-" + mutationRate + ".txt", "Time taken : " + time_taken + "\n", (err) => {
        if (err) throw err;
    });
    return population;
}

async function objectiveFunction(assignments, volunteers, slots){
    let score = initialScore;
    for (let i = 0; i < assignments.length; i++) {
        //fs.appendFile('logs.txt',"Penalties for assignment number " + i + " \n", "", (err) => {
        //    if (err) throw err;
        //})
        try {
            //fs.appendFile('logs.txt',"Name : " + assignments[i].slot.name + " from " + new Date(0).setUTCMilliseconds(assignments[i].slot.start) + " " + new Date(0).setUTCMilliseconds(assignments[i].slot.end) + "\n", "", (err) => {
            //    if (err) throw err;
            //})
        } catch (e) {
            console.log(e);
        }
        if(!isAVolunteerAssigned(assignments[i])){
            score -= noVolunteerAssigned;
            //fs.appendFile('logs.txt',"No volunteer assigned \n", "", (err) => {
            //    if (err) throw err;
            //})
        } else {
            if (!hasUserEnoughRole(assignments[i].volunteer, assignments[i].slot.requiredTeam)) {
                score = score - notNimimumRolePenalty;
                //fs.appendFile('logs.txt',"Wrong role : " + assignments[i].volunteer.firstname + " " + assignments[i].volunteer.lastname + " should be " + assignments[i].slot.requiredTeam + "\n", "", (err) => {
                //    if (err) throw err;
                //})
            }

            if (isAvailable(getObjectById(volunteers, assignments[i].volunteer.id), assignments[i].slot) == null) {
                score -= notAvailablePenalty;
                //fs.appendFile('logs.txt',"Not available : " + assignments[i].volunteer.firstname + " " + assignments[i].volunteer.lastname + " \n", "", (err) => {
                //    if (err) throw err;
                //})
            }
            if (isConflictdOnSlot(assignments[i], assignments)) {
                score -= conflictPenalty;
                //fs.appendFile('logs.txt', "Has conflict : " + assignments[i].volunteer.firstname + " " + assignments[i].volunteer.lastname + " " + assignments[i].slot.start + " " + assignments[i].slot.end + "\n", "", (err) => {
                //    if (err) throw err;
                //})
            }
            if (isWrongRequiredVolunteer(assignments[i], volunteers)) {
                score -= notRequiredVolunteerPenalty;
                //fs.appendFile('logs.txt',"Wrong volunteer for slot \n", "", (err) => {
                //    if (err) throw err;
                //})
            }

            //fs.appendFile('logs.txt',"********** \n", "", (err) => {
            //    if (err) throw err;
            //})
        }
    }
    return score;
}

//Import DB
// let volunteers = setUsers().then(volunteers => {
//     console.log(volunteers);
//     return volunteers;
//
// }).then(volunteers => setSlots().then(slots => {
//     console.log(slots);
//     console.log(slots[0].needs);
//     return {slots, volunteers};
// })).then(({ slots, volunteers } )=> {
//     assAll = autoAssign(slots, volunteers);
//     return assAll;
//     //ass1 = assignSlotToVolunteer(1, volunteers[0].id, slots[0].FTid, slots[0].name, slots[0].start, slots[0].end);
// }).then(assAll => {
//     //console.log(assAll);
// });

//User local DB (volunteers.json)
//require('./volunteers.json')
let volunteers = setUsers().then(volunteers => {
    return volunteers;

}).then(volunteers => setSlots().then(slots => {
    //console.log(slots);
    return {slots, volunteers};
})).then(({ slots, volunteers } )=> {
    console.log(slots.length);
    //let binary = geneticAlgorithm(volunteers, slots, population_size, generations, recombination_rate, mutation_rate);
    assAll = autoAssign(slots, volunteers);
    assAll = "";
    binary = "";
    return {assAll, binary};
}).then(({assAll, binary}) => {
});
